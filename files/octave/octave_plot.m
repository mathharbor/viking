t = 0:0.1:1
r = linspace (0, 1, numel (t));
z = linspace (0, 1, numel (t));
p = plot3 (r.*sin(t), r.*cos(t), z);

test