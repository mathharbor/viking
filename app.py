import os
import os.path as op
import subprocess

from flask import Flask, url_for, redirect, render_template, request

from wtforms import form, fields, validators
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext import admin, login
from flask.ext.admin import BaseView, AdminIndexView, expose
from flask.ext.admin.contrib import fileadmin
from flask.ext.admin import helpers
from flask_debugtoolbar import DebugToolbarExtension
import redis

# Create flask app
app = Flask(__name__, template_folder='templates', static_folder='files')

# Create dummy secrey key so we can use flash
app.config['SECRET_KEY'] = 'ckxvjkjcvh834798754ujovbowiwtvoe8to'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.sqlite'
app.debug = False
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
app.config['DEBUG_TB_PROFILER_ENABLED'] = True
#toolbar = DebugToolbarExtension(app)
#app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120))
    password = db.Column(db.String(64))

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username

# Define login and registration forms (for flask-login)
class LoginForm(form.Form):
    login = fields.TextField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise validators.ValidationError('Invalid user')

        if user.password != self.password.data:
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(User).filter_by(login=self.login.data).first()

class RegistrationForm(form.Form):
    login = fields.TextField(validators=[validators.required()])
    email = fields.TextField()
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        if db.session.query(User).filter_by(login=self.login.data).count() > 0:
            raise validators.ValidationError('Duplicate username')


class ContainerForm(form.Form):
    memory = fields.TextField()



def init_login():
    login_manager = login.LoginManager()
    login_manager.setup_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)

class MyFileAdmin(fileadmin.FileAdmin):
    edit_template = 'file_edit.html'
    def is_accessible(self):
        return login.current_user.is_authenticated()

    def get_base_path(self):
        path = fileadmin.FileAdmin.get_base_path(self)
        if not login.current_user.is_anonymous():
            workspace_name = login.current_user.login
            custom_path = '/%s/' %workspace_name
            return path + custom_path
        else:
            return path

    def get_base_url(self):
        url = fileadmin.FileAdmin.get_base_url(self)
        if not login.current_user.is_anonymous():
            workspace_name = login.current_user.login
            custom_url = '%s/' %workspace_name
            return url + custom_url
        else:
            return url

class User_Logout(BaseView):
    def is_accessible(self):
	return login.current_user.is_authenticated()
   
    @expose('/')
    def index(self):
	return redirect('/logout')


class IndexView(AdminIndexView):
    def is_accessible(self):
        return login.current_user.is_authenticated()

    @expose('/')
    def index(self):
        return self.render('home.html')

class Runtime_Sandbox(BaseView):
    def is_accessible(self):
	return login.current_user.is_authenticated()

    @expose('/')
    def index(self):
        #form = ContainerForm(request.form)
        #if request.form:
	#c_memory = form.memory.data
        #print form.memory.data
        username = login.current_user.login
        c_volume = '/root/viking/files/%s/' %username
        x = subprocess.check_output("docker run -d -i -t -v %s:/files mathharbor/platform /bin/bash" %c_volume, shell=True)
        container_id = x.strip('\n')
        return self.render('terminal.html', container_id=container_id)
        
#return self.render('container_form.html', form=form)

class Terminal(BaseView):
    @expose('/')
    def index(self):
        return self.render('terminal.html')

# Flask views

@app.route('/')
def index():
    return render_template('index.html', user=login.current_user)


@app.route('/login/', methods=('GET', 'POST'))
def login_view():
    form = LoginForm(request.form)
    if request.form:
        user = form.get_user()
        login.login_user(user)
        return redirect('/admin')

    return render_template('form.html', form=form)


@app.route('/register/', methods=('GET', 'POST'))
def register_view():
    form = RegistrationForm(request.form)
    if request.form:
        user = User()

        form.populate_obj(user)
        workspace_name = user.login
        os.chdir('files')
        os.mkdir(workspace_name)
        os.chdir('../')
        db.session.add(user)
        db.session.commit()

        login.login_user(user)
        return redirect('/admin')

    return render_template('form.html', form=form)


@app.route('/logout/')
def logout_view():
    login.logout_user()
    return redirect(url_for('index'))

if __name__ == '__main__':
    init_login()
    '''
    try:
        os.mkdir(bp)
    except OSError:
        pass
    '''

    # Create admin interface
    path = op.join(op.dirname(__file__), 'files')
    admin = admin.Admin(app, "MathHarbor - Dashboard", index_view=IndexView())
    admin.add_view(MyFileAdmin(path, '/files/', name='Files'))
    admin.add_view(Runtime_Sandbox(name='Terminal'))
    admin.add_view(User_Logout(name='Logout'))
    #admin.add_view(Terminal())

    # Start app
    app.run(debug=False, host='0.0.0.0', port=80)
