import docker

c_memory = 530000
c_image = 'mathharbor/ipyv4'
command = '/bin/bash'
c_volume = '/files/rudimk/'
c = docker.Client(base_url='unix://var/run/docker.sock', version="1.4")
new_container = c.create_container(image=c_image, command=command, mem_limit=c_memory, volumes={"/files" : {} })
new_container_id = new_container["Id"]
c.start(new_container, binds={"/files" : c_volume})
